'use strict';
const electron = require('electron');

const ipc = electron.ipcRenderer;
const configStore = electron.remote.require('./config');

function updateZoomLevel() {
  electron.webFrame.setZoomLevel(configStore.get('zoomLevel'));
}

ipc.on('updateZoomLevel', updateZoomLevel);

document.addEventListener('DOMContentLoaded', () => {
  updateZoomLevel();
});
