# <img src="media/logo.png" width="45" align="left">&nbsp;WhatsAppLinux

> Unofficial WhatsApp app

[![](media/screenshot.png)](https://gitlab.com/dslak/whatsapp-linux)



## Build

Built with [Electron](http://electron.atom.io).

###### Commands

- Init: `$ npm install`
- Run: `$ npm start`
- Build Linux: `$ npm run build


