'use strict';
const {app, BrowserWindow, Menu, shell} = require('electron');
const os = require('os');
const configStore = require('./config');

const appName = app.getName();

function restoreWindow() {
  const win = BrowserWindow.getAllWindows()[0];
  win.show();
  return win;
}

function sendAction(action) {
  const win = BrowserWindow.getAllWindows()[0];
  win.webContents.send(action);
}

const trayTpl = [
  {
    label: 'Show',
    click() {
      restoreWindow();
    }
  },
  {
    type: 'separator'
  },
  {
    label: `Quit ${appName}`,
    click() {
      app.exit(0);
    }
  }
];

const viewTpl = {
  label: 'View',
  submenu: [
    {
      label: 'Reset Text Size',
      accelerator: 'CmdOrCtrl+0',
      click() {
        configStore.set('zoomLevel', 0);
        sendAction('updateZoomLevel');
      }
    },
    {
      label: 'Increase Text Size',
      accelerator: 'CmdOrCtrl+Plus',
      click() {
        configStore.set('zoomLevel', configStore.get('zoomLevel') + 1);
        sendAction('updateZoomLevel');
      }
    },
    {
      label: 'Decrease Text Size',
      accelerator: 'CmdOrCtrl+-',
      click() {
        configStore.set('zoomLevel', configStore.get('zoomLevel') - 1);
        sendAction('updateZoomLevel');
      }
    }
  ]
};

const linuxTpl = [
  {
    label: 'Edit',
    submenu: [
      {
        label: 'Cut',
        accelerator: 'CmdOrCtrl+X',
        role: 'cut'
      },
      {
        label: 'Copy',
        accelerator: 'CmdOrCtrl+C',
        role: 'copy'
      },
      {
        label: 'Paste',
        accelerator: 'CmdOrCtrl+V',
        role: 'paste'
      }
    ]
  },
  viewTpl,
  {
    label: 'Help',
    role: 'help'
  }
];

let tpl = linuxTpl;


module.exports = {
  mainMenu: Menu.buildFromTemplate(tpl),
  trayMenu: Menu.buildFromTemplate(trayTpl)
};
